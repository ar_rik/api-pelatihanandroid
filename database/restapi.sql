-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 05, 2016 at 05:32 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor1` float NOT NULL COMMENT 'pH',
  `sensor2` float NOT NULL COMMENT 'Suhu',
  `sensor3` float NOT NULL COMMENT 'Do',
  `output` float NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `sensor1`, `sensor2`, `sensor3`, `output`, `status`, `created_at`) VALUES
(19, 11, 21, 31, 41, 0, '2016-03-05 10:04:38'),
(20, 312241, 3122120, 2153210, 98712, 0, '2016-03-05 10:11:08'),
(21, 1, 2, 3, 4, 0, '2016-03-05 10:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password_hash`, `api_key`, `status`, `created_at`) VALUES
(2, 'Ria Arni', 'riaarni@email.com', '$2a$10$b9d080e261852e54b6d81uffRsx3B3DrESGocS9vs/D8azCNkzwnS', 'aabb40e1047ad3825a2260b6197a38c3', 1, '2016-02-29 01:27:48'),
(3, 'Ardhi Maarik', 'ardhimaarik@yahoo.co.id', '$2a$10$c8fa619dd54b2add764f4exbAtbNo4POfNwJRlElGl/TrImEaLwFa', '2a97e2eb08ae6f5ec1f10426fd849bb1', 1, '2016-02-29 01:28:29'),
(4, 'maarik', 'ardhi@email.com', '$2a$10$7a51d3d4466292c5c2aeeOik65ZQVu6IH3ZU.Wmy0XzoveWMDlAeO', 'b7fb740f0de854331fd150afcd4845e9', 1, '2016-03-03 04:18:02'),
(7, 'ArdhiMaarikaa', 'ardhimaarikaa@yahoo.co.id', '$2a$10$3fc925f4b1d71118518fauq19f83562MQQH5C7FH5EEx2C0Y59j6O', '088a8138549146e2456ecf25583b9a8b', 1, '2016-03-03 08:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_tasks`
--

CREATE TABLE IF NOT EXISTS `user_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `user_tasks`
--

INSERT INTO `user_tasks` (`id`, `user_id`, `task_id`) VALUES
(19, 3, 19),
(20, 3, 20),
(21, 3, 21);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_tasks`
--
ALTER TABLE `user_tasks`
  ADD CONSTRAINT `user_tasks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_tasks_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
