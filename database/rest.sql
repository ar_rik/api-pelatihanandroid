-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 03, 2016 at 11:13 AM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor1` float NOT NULL COMMENT 'pH',
  `sensor2` float NOT NULL COMMENT 'Suhu',
  `sensor3` float NOT NULL COMMENT 'Do',
  `output` float NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `sensor1`, `sensor2`, `sensor3`, `output`, `status`, `created_at`) VALUES
(124, 1233, 1234, 1234, 12345, 0, '2016-03-09 03:27:42'),
(125, 12334400, 1234, 1234, 12345, 0, '2016-03-09 03:27:59'),
(126, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:32:37'),
(127, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:35:06'),
(128, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:37:50'),
(129, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:40:05'),
(130, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:40:35'),
(131, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:40:56'),
(132, 1233440, 1234, 1234, 12345, 0, '2016-03-09 03:41:04'),
(133, 1, 1234, 1234, 12345, 0, '2016-03-09 03:42:19'),
(134, 10, 1234, 1234, 12345, 0, '2016-03-09 03:42:52'),
(135, 11, 1234, 1234, 12345, 0, '2016-03-09 03:43:55'),
(136, 11, 1234, 1234, 12345, 0, '2016-03-09 03:44:17'),
(137, 15, 1234, 1234, 12345, 0, '2016-03-09 03:44:31'),
(138, 16, 1234, 1234, 12345, 0, '2016-03-09 03:44:36'),
(139, 16, 1234, 1234, 12345, 0, '2016-03-09 03:48:21'),
(140, 16, 1234, 1234, 12345, 0, '2016-03-09 03:48:55'),
(141, 16, 1234, 1234, 12345, 0, '2016-03-09 03:49:44'),
(142, 1233, 1234, 1234, 12345, 0, '2016-03-10 23:44:32'),
(143, 1233, 1234, 1234, 12345, 0, '2016-03-10 23:46:30'),
(144, 1233, 1234, 1234, 12345, 0, '2016-03-10 23:46:39'),
(145, 1233, 1234, 1234, 12345, 0, '2016-03-10 23:47:03'),
(146, 1233, 1234, 1234, 12345, 0, '2016-03-10 23:47:34'),
(147, 1233, 1234, 1234, 12345, 0, '2016-03-11 00:23:52'),
(148, 1233, 1234, 1234, 12345, 0, '2016-03-11 00:30:46'),
(149, 5555, 5555, 5555, 5555, 0, '2016-03-16 13:48:43'),
(150, 5555, 5555, 5555, 5555, 0, '2016-03-16 13:48:57'),
(151, 222, 222, 222, 222, 0, '2016-03-16 13:49:21'),
(152, 222, 222, 222, 222, 0, '2016-03-16 13:49:42'),
(153, 222, 222, 222, 222, 0, '2016-03-16 13:49:53'),
(154, 222, 222, 222, 222, 0, '2016-03-16 13:50:01'),
(155, 987, 987, 987, 987, 0, '2016-03-17 01:26:19'),
(156, 987, 987, 987, 987, 0, '2016-03-17 01:26:22'),
(157, 987, 987, 987, 987, 0, '2016-03-17 01:26:23'),
(158, 987, 987, 987, 987, 0, '2016-03-17 01:26:25'),
(159, 987, 987, 987, 987, 0, '2016-03-17 01:26:27'),
(160, 987, 987, 987, 987, 0, '2016-03-17 01:26:28'),
(161, 987, 987, 987, 987, 0, '2016-03-17 01:26:30'),
(162, 987, 987, 987, 987, 0, '2016-03-17 01:26:32'),
(163, 987, 987, 987, 987, 0, '2016-03-17 01:26:33'),
(164, 987, 987, 987, 987, 0, '2016-03-17 01:26:35'),
(165, 987, 987, 987, 987, 0, '2016-03-17 01:26:36'),
(166, 1, 2, 3, 4, 0, '2016-05-03 03:55:12'),
(167, 1, 2, 3, 4, 0, '2016-05-03 03:55:46'),
(168, 1, 2, 3, 4, 0, '2016-05-03 04:00:08'),
(169, 1, 2, 3, 4, 0, '2016-05-03 04:01:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `gcm_registration_id` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password_hash`, `api_key`, `gcm_registration_id`, `status`, `created_at`) VALUES
(2, 'Ria Arni', 'riaarni@email.com', '$2a$10$b9d080e261852e54b6d81uffRsx3B3DrESGocS9vs/D8azCNkzwnS', 'aabb40e1047ad3825a2260b6197a38c3', '', 1, '2016-02-29 01:27:48'),
(3, 'Ardhi Maarik', 'ardhimaarik@yahoo.co.id', '$2a$10$c8fa619dd54b2add764f4exbAtbNo4POfNwJRlElGl/TrImEaLwFa', '2a97e2eb08ae6f5ec1f10426fd849bb1', '', 1, '2016-02-29 01:28:29'),
(4, 'maarik', 'ardhi@email.com', '$2a$10$7a51d3d4466292c5c2aeeOik65ZQVu6IH3ZU.Wmy0XzoveWMDlAeO', 'b7fb740f0de854331fd150afcd4845e9', '', 1, '2016-03-03 04:18:02'),
(7, 'ardhiMaarrik', 'ardhimaarikaa@yahoo.co.id', '$2a$10$3fc925f4b1d71118518fauq19f83562MQQH5C7FH5EEx2C0Y59j6O', '088a8138549146e2456ecf25583b9a8b', '', 1, '2016-03-03 08:31:49'),
(33, 'ardhiMaarik', 'ardhi@gmail.com', '$2a$10$ff5d0c46aa4fed7b628f1uEgI0CV87/97U.DezVpzvKWnDh.2w4pa', '746be99d4ef39262e059f3afb7a9cb4f', 'eRzwK0G-R24:APA91bE2KZjtEp0_Po-BO4ktyW8c9mqdQ0rO_RU_tsD06tl9Of2JuoUBGJpqDcj5VfSvgagtmwr5IuS7gfFgsFYTpz56hs7VkUheJCTD6X_tSZiv5mgH4IOAIMJvbxnqwZ8_Zu1bYwMI', 1, '2016-03-06 23:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_tasks`
--

CREATE TABLE IF NOT EXISTS `user_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

--
-- Dumping data for table `user_tasks`
--

INSERT INTO `user_tasks` (`id`, `user_id`, `task_id`) VALUES
(124, 33, 124),
(125, 33, 125),
(126, 33, 126),
(127, 33, 127),
(128, 33, 128),
(129, 33, 129),
(130, 33, 130),
(131, 33, 131),
(132, 33, 132),
(133, 33, 133),
(134, 33, 134),
(135, 33, 135),
(136, 33, 136),
(137, 33, 137),
(138, 33, 138),
(139, 33, 139),
(140, 33, 140),
(141, 33, 141),
(142, 33, 142),
(143, 33, 143),
(144, 33, 144),
(145, 33, 145),
(146, 33, 146),
(147, 33, 147),
(148, 33, 148),
(149, 33, 149),
(150, 33, 150),
(151, 33, 151),
(152, 33, 152),
(153, 33, 153),
(154, 33, 154),
(155, 33, 155),
(156, 33, 156),
(157, 33, 157),
(158, 33, 158),
(159, 33, 159),
(160, 33, 160),
(161, 33, 161),
(162, 33, 162),
(163, 33, 163),
(164, 33, 164),
(165, 33, 165),
(166, 33, 166),
(167, 33, 167),
(168, 33, 168),
(169, 33, 169);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_tasks`
--
ALTER TABLE `user_tasks`
  ADD CONSTRAINT `user_tasks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_tasks_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
